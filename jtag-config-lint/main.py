import argparse
import yaml
import json
import jsonschema
import os
import re

# format the given value as a hexadecimal string with as many
# leading zeros as can be represented with the given width
def format_hex(value, width):
    format_str = "0x{{:0{}x}}".format((width+4-1)//4)
    return format_str.format(value)

# list supported syntax versions based on what json schema files
# exist in schemas/
def get_supported_versions():
    pattern = re.compile(r"^v([1-9]+[0-9]*)\.json$")
    schemafiles = os.listdir("schemas")
    matches = [pattern.match(s) for s in schemafiles]
    versions = []
    for m in matches:
        try:
            versions.append(m.group(1))
        except Exception as e:
            print(e)
    return versions

# try to get syntax version as int from loaded yaml config
# abort program execution if the syntax version is invalid
def get_syntax_version(config):
    if not "syntax_version" in config:
        print("Missing required top-level key: `syntax_version`.")
        exit(-1)
    syntax_version = config['syntax_version']
    try:
        syntax_version = int(syntax_version)
    except:
        print("Top-level key `syntax_version` must have a positive integer value, `{}` is not.".format(syntax_version))
        exit(-1)
    if syntax_version <= 0:
        print("Top-level key `syntax_version` must have a positive integer value, `{}` is not.".format(syntax_version))
        exit(-1)
    return syntax_version

# check for duplicate values in ir fields, and print verbose
# diagnostics for all conflicts
def disallow_duplicate_irs(config):
    duplicate_ir = False
    for i in range(len(config['tdrs'])):
        tdr_1 = config['tdrs'][i]
        ir_1 = tdr_1['ir']
        conflicts = []
        for j in range(i, len(config['tdrs'])):
            tdr_2 = config['tdrs'][j]
            ir_2 = tdr_2['ir']
            if tdr_1 is not tdr_2 and ir_1 == ir_2:
                conflicts.append(tdr_2)
                duplicate_ir = True
        if len(conflicts) > 0:
            ir_hex = format_hex(ir_1, config['ir_width'])
            print("Conflicting values for ir={} in registers:".format(ir_hex))
            print("  - {}".format(tdr_1['name']))
            for c in conflicts:
                print("  - {}".format(c['name']))
    if duplicate_ir:
        print("Each entry in `tdrs` must have an unique value for the field `ir`.")
        exit(-1)

def disallow_unrepresentable_irs(config):
    ir_width = config['ir_width']
    max_ir = 2**ir_width-1
    for (i, tdr) in enumerate(config['tdrs']):
        ir = tdr['ir']
        if not (0 <= ir <= max_ir):
            hex_ir = format_hex(ir, ir_width)
            print("The requested ir={} cannot be represented in a {}-bit wide unsigned integer as configured by `ir_width: {}`."
                .format(hex_ir, ir_width, ir_width))
            print("  {}".format(tdr))

# user configured tdrs shall not use the ir value reserved for BYPASS instruction
# where every ir bit is set to one
def disallow_bypass_ir_value(config):
    ir_width = config['ir_width']
    max_ir = 2**ir_width-1
    for (i, tdr) in enumerate(config['tdrs']):
        ir = tdr['ir']
        if ir == max_ir:
            print("The ir value {} is reserved for the BYPASS instruction".format(format_hex(ir, ir_width)))
            print("  {}".format(tdr))



# read the contents of the appropriate jtag config schema file
def read_schema(version):
    with open("schemas/v{}.json".format(version), "r") as fd:
        file_contents = fd.read()
        return json.loads(file_contents)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('config_file')
    args = parser.parse_args()

    filename = args.config_file
    with open(filename, "r") as fd:
        config = yaml.load(fd.read(), yaml.Loader)
        supported_versions = get_supported_versions()
        syntax_version = get_syntax_version(config)
        schema = read_schema(syntax_version)

        result = None
        try: 
            result = jsonschema.validate(config, schema, jsonschema.Draft202012Validator)
        except jsonschema.exceptions.ValidationError as e:
            print(e)
            print("The given file does not conform to syntax version {} schema.".format(syntax_version))
            exit(-1)

        disallow_duplicate_irs(config)
        disallow_unrepresentable_irs(config)
        disallow_bypass_ir_value(config)

        print("No issues were found in file: {}".format(filename))

if __name__=='__main__':
    main()

