// See LICENSE_AALTO.txt for license details

package jtag.config

import org.scalatest._
import net.jcazevedo.moultingyaml._
import scala.io.Source
import scala.math.pow
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import Helpers._

/** syntax_version parsing checks */
class ParseSyntaxVersionSpec extends AnyFlatSpec with Matchers {
  it should "not parse config with missing syntax_version key" in {
    val source = "syntax_verison: 3"  // e.g. syntax_version contains a typo
    val yamlAst = source.parseYaml
    val result = JtagConfig.parseSyntaxVersion(yamlAst) match {
      case Right(err) => assertThrows[JtagConfig.JtagConfigParseException] { err.except() }
      case Left(version) => assert(false)
    }
  }
  it should "not parse config with missing syntax_version value" in {
    val source = "syntax_version:"
    val yamlAst = source.parseYaml
    val result = JtagConfig.parseSyntaxVersion(yamlAst) match {
      case Right(err) => assertThrows[JtagConfig.JtagConfigParseException] { err.except() }
      case Left(version) => assert(false)
    }
  }
  it should "not parse config with non-number syntax_version value" in {
    val source = "syntax_version: foo"
    val yamlAst = source.parseYaml
    val result = JtagConfig.parseSyntaxVersion(yamlAst) match {
      case Right(err) => assertThrows[JtagConfig.JtagConfigParseException] { err.except() }
      case Left(version) => assert(false)
    }
  }
  it should "not parse config with decimal syntax_version value" in {
    val source = "syntax_version: 1.2"
    val yamlAst = source.parseYaml
    val result = JtagConfig.parseSyntaxVersion(yamlAst) match {
      case Right(err) => assertThrows[JtagConfig.JtagConfigParseException] { err.except() }
      case Left(version) => assert(false)
    }
  }
  it should "not parse config with invalid syntax_version" in {
    val source = "syntax_version: -1"
    val yamlAst = source.parseYaml
    val result = JtagConfig.parseSyntaxVersion(yamlAst) match {
      case Right(err) => assertThrows[JtagConfig.JtagConfigParseException] { err.except() }
      case Left(version) => assert(false)
    }
  }
  it should "parse syntax version successfully with a valid syntax version" in {
    // test all supported versions
    val source = s"syntax_version: ${JtagConfig.syntaxVersion}"
    val yamlAst = source.parseYaml
    val result = JtagConfig.parseSyntaxVersion(yamlAst) match {
      case Left(version) => { assertResult(JtagConfig.syntaxVersion) { version } }
      case Right(err) => assert(false)
    }
  }
}

// main class for running just the JtagConfig parser
// using resources defined in src/test/resources
// invoke from sbt with:
//   test:runMain jtag.config.ConfigParserTestMain
object ConfigParserTestMain extends App {
  val source = getResourceString("jtag-config.yml")
  val yamlAst = source.parseYaml
  val config = yamlAst.convertTo[JtagConfig]
  println(source)
  println(config)
}

object Helpers {
  // read contents of a resource file to a string
  def getResourceString(resource: String): String = {
    val bufferedSource = Source.fromResource(resource)
    val source: String = bufferedSource.getLines().mkString("\n")
    bufferedSource.close
    source
  }
}
