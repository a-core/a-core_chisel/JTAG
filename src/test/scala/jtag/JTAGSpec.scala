// See LICENSE_AALTO.txt for license details

package jtag

import chisel3._
import chisel3.util._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec
import jtag._
import BinaryParse._

class JTAGSpec extends AnyFlatSpec with JtagTestUtilities with ChainIOUtils with ChiselScalatestTester {

  //JtagShifterSpec

  "JTAG bypass chain" should "work" in {
    test(JtagBypassChain()) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      nop(c.io)
      step()


      // Shift without expect
      c.io.chainIn.shift.poke(1.U)
      c.io.chainIn.data.poke(0.U)
      step()

      // Test shift functionality
      shift(c.io, 0, 0)
      step()

      shift(c.io, 0, 1)
      step()

      shift(c.io, 1, 1)
      step()

      shift(c.io, 1, 0)
      step()

      shift(c.io, 0, 0)
      step()

      c.io.chainIn.shift.poke(1.U)
      c.io.chainOut.shift.expect(1.U)
      c.io.chainOut.capture.expect(0.U)
      c.io.chainOut.update.expect(0.U)

      c.io.chainIn.data.poke(0.U)
      step()
      c.io.chainOut.data.expect(0.U)

      c.io.chainIn.data.poke(1.U)
      step()
      c.io.chainOut.data.expect(1.U)

      c.io.chainIn.data.poke(1.U)
      step()
      c.io.chainOut.data.expect(1.U)

      c.io.chainIn.data.poke(0.U)
      step()
      c.io.chainOut.data.expect(0.U)

      c.io.chainIn.data.poke(1.U)
      step()
      c.io.chainOut.data.expect(1.U)
    }
  }

  "8-bit capture chain" should "work" in {
    test(CaptureChain(UInt(8.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      // Test capture and shift
      c.io.capture.bits.poke("b01001110".U)
      capture(c.io)
      step()
      step()

      shift(c.io, 0, 1)
      step()

      shift(c.io, 1, 1)
      step()

      shift(c.io, 1, 0)
      step()

      shift(c.io, 1, 1)
      step()

      shift(c.io, 0, 1)
      step()
    }
  }

  "8-bit capture-update chain" should "work" in {
    test(CaptureUpdateChain(UInt(8.W))).withAnnotations(Seq(WriteVcdAnnotation)) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      // Test capture and shift
      c.io.capture.bits.poke("01001110".b)
      capture(c.io)
      step()
      step()

      c.io.chainIn.capture.poke(0.U)
      c.io.chainIn.shift.poke(1.U)
      c.io.chainOut.shift.expect(1.U)
      c.io.chainOut.capture.expect(0.U)
      c.io.chainOut.update.expect(0.U)
      c.io.capture.capture.expect(0.U)
      c.io.update.valid.expect(0.U)

      c.io.update.bits.expect("b01001110".U)  // whitebox testing
      shift(c.io, 0, 1)
      step()

      c.io.update.bits.expect("b10100111".U)  // whitebox testing
      shift(c.io, 1, 1)
      step()

      nop(c.io)  // for giggles
      step()

      c.io.update.bits.expect("b11010011".U)  // whitebox testing
      shift(c.io, 1, 0)
      step()

      c.io.update.bits.expect("b01101001".U)  // whitebox testing
      shift(c.io, 1, 1)
      step()

      // Test update
      update(c.io)
      c.io.update.bits.expect("b10110100".U)
      step()

      // Capture-idle-update tests
      c.io.capture.bits.poke("b00000000".U)
      capture(c.io)
      step()
      step()

      nop(c.io)
      step()
      nop(c.io)
      step()
      nop(c.io)
      step()
      nop(c.io)
      step()

      update(c.io)
      c.io.update.bits.expect("b00000000".U)
      step()

      // Capture-update tests
      c.io.capture.bits.poke("b11111111".U)
      capture(c.io)
      step()
      step()

      update(c.io)
      c.io.update.bits.expect("b11111111".U)
      step()
    }
  }

  "Vector and Bundle capture-update chains" should "be ordered properly" in {
    class InnerBundle extends Bundle {
      val d = Bool()
      val c = UInt(2.W)
      
    }

    class TestBundle extends Bundle {
      val b = UInt(2.W)
      val a = Bool()  // this is second to ensure Bundle order is preserved
      val c = Vec(3, Bool())
      val x = new InnerBundle()
      // Ordering should be:
      // b[1:0] a c[2] c[1] c[0] x.d x.c[1:0]
    }
    test(CaptureUpdateChain(gen = new TestBundle())) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      val cap = c.io.capture.bits
      val upd = c.io.update.bits

      // Test capture, shift, and update
      cap.b.poke("b01".U)
      cap.a.poke(1.U)
      cap.c(2).poke(1.U)
      cap.c(1).poke(0.U)
      cap.c(0).poke(0.U)
      cap.x.d.poke(1.U)
      cap.x.c.poke("b01".U)

      capture(c.io)
      step()
      step()

      shift(c.io, 1, 1)  // capture LSB, update LSB
      step()
      shift(c.io, 0, 1)
      step()
      shift(c.io, 1, 0)
      step()
      shift(c.io, 0, 0)
      step()
      shift(c.io, 0, 0)
      step()
      shift(c.io, 1, 1)
      step()
      shift(c.io, 1, 0)
      step()
      shift(c.io, 1, 0)
      step()
      shift(c.io, 0, 1)  // capture MSB, update MSB
      step()

      update(c.io)
      upd.b.expect("b10".U)
      upd.a.expect(0.U)
      upd.c(2).expect(1.U)
      upd.c(1).expect(0.U)
      upd.c(0).expect(0.U)
      upd.x.d.expect(0.U)
      upd.x.c.expect("b11".U)
      step()
    }
  }

  "Capture-update chain with larger capture width" should "work" in {
    test(CaptureUpdateChain(UInt(4.W), UInt(2.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      c.io.capture.bits.poke("b1101".U)
      capture(c.io)
      step()
      step()

      shift(c.io, 1, 0)  // capture LSB, update LSB
      step()
      shift(c.io, 0, 1)  // update MSB
      step()
      shift(c.io, 1, 1)
      step()
      shift(c.io, 1, 1)  // capture MSB
      step()

      update(c.io)
      c.io.update.bits.expect("b10".U)
    }
  }

  "Capture-update chain with larger update width" should "work" in {
    test(CaptureUpdateChain(UInt(2.W), UInt(4.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      c.io.capture.bits.poke("b10".U)
      capture(c.io)
      step()
      step()

      shift(c.io, 0, 1)  // capture LSB, update LSB
      step()
      shift(c.io, 1, 0)  // capture MSB
      step()
      shift(c.io, 0, 1)
      step()
      shift(c.io, 0, 1)  // update MSB
      step()

      update(c.io)
      c.io.update.bits.expect("b1101".U)
    }
  }

}


/** TODO: Port old unit tests below to use chiseltest **/
/** JTAG unit tester 
  * Unit test of the JTAG module where 5 Test Data registers are created. This test simultaneously read and writes the value of a 48bit register
  * from the value 3521 to value 43, at the end the read/write flag register is written to 0
  */
// class unit_tester(c: JTAG) extends DspTester(c) {
//   def cycle(tms: Int, tdi: Int){
//     poke(c.io.TCK, 0)
//     step(1)
//     poke(c.io.TMS, tms)
//     poke(c.io.TDI, tdi)
//     poke(c.io.TCK, 1)
//     step(1)
//     poke(c.io.TMS, tms)
//   }
// 
//   //tmsReset
//   poke(c.io.TMS, 1)
//   poke(c.io.TCK, 1)
//   step(1)
//   for (_ <- 0 until 5) {
//     poke(c.io.TCK, 0)
//     step(1)
//     poke(c.io.TCK, 1)
//     step(1)
//   } 
// 
//   expect(c.debug.get.state, 15) //TestLogicReset [ 15 ]
// 
//   cycle(0,0) //Cycle TMS 0
//   
//   expect(c.debug.get.state, 12) //RunTestIdle [ 12 ]
// 
//   println("---TDR TEST START---")
//   
//   poke(c.tdrio.in(3), 3521)
// 
//   cycle(1,0) // TMS 1
//   cycle(1,0) // TMS 1
//   cycle(0,0) // TMS 0
//   cycle(0,0) // TMS 0
//    
//   expect(c.debug.get.state, 10) //ShiftIR [ 10 ]
// 
//   cycle(0,1) // TMS 0
//   expect(c.io.TDO.data, 1) //1
//   cycle(0,1) // TMS 0 
//   expect(c.io.TDO.data, 0) //0
//   cycle(0,0) // TMS 0
//   expect(c.io.TDO.data, 0) //0
//   cycle(0,0) // TMS 0 
//   expect(c.io.TDO.data, 0) //0
//   cycle(0,0) // TMS 0
//   expect(c.io.TDO.data, 0) //0
//   cycle(0,0) // TMS 0 
//   expect(c.io.TDO.data, 0) //0
//   cycle(0,0) // TMS 0 
//   expect(c.io.TDO.data, 0) //0
//   cycle(0,0) // TMS 0 
//   expect(c.io.TDO.data, 0) //0
// 
//   cycle(1,0) // TMS 1 
//   expect(c.io.TDO.data, 0) //0
// 
//   //exit shifting
//   cycle(1,0) // TMS 1
//   cycle(0,0) // TMS 0
// 
//   expect(c.debug.get.state, 12) //RunTestIdle [ 12 ]
//   expect(c.debug.get.instruction, 3) //TDR instruction
// 
//   cycle(1,0) // TMS 1
//   cycle(0,0) // TMS 0
//   cycle(0,0) // TMS 0
//   
//   expect(c.debug.get.state, 2) //DRShift [ 2 ]
//   expect(c.tdrio.out(3), 3521) //0b110111000001 = 3521  
//    
//   for (i <- 0 until 48) {       
//     if (i == 0 || i == 1 || i == 3|| i == 5){ //0b00101011 = 43
//       cycle(0,1) //1
//     } else {
//       cycle(0,0) // TMS 0
//     }
//     if(i == 0 || i == 6 || i == 7 || i == 8 || i == 10 || i == 11 ){ //0b110111000001 = 3521
//       expect(c.io.TDO.data, 1) //1
//     } else {
//       expect(c.io.TDO.data, 0) //0
//     }
//   }
// 
//   cycle(1,0) // TMS 1
//   expect(c.io.TDO.data, 0) //0
// 
//   //exit shifting
//   cycle(1,0) // TMS 1
//   cycle(0,0) // TMS 0
// 
//   expect(c.debug.get.state, 12) //RunTestIdle [ 12 ]
//   expect(c.tdrio.out(3), 43)
// 
//   println("---MEM TDR TEST DONE---")
// }
// 
// /** Unit test driver */
// object unit_test extends App with OptionParser {
//   // Parse command-line arguments
//   val (options, arguments) = getopts(default_opts, args.toList)
//   printopts(options, arguments)
// 
//   iotesters.Driver.execute(arguments.toArray, () => new JTAG(
//     config = JtagConfig(
//       syntax_version=None,
//       ir_width=8,
//       id_code=Some(IdcodeConfig(1,1,1)),
//       tdrs=Seq(
//         TdrConfig(name="debug_5", ir=5, width=16, chain_type=ChainType.UpdateChain),
//         TdrConfig(name="debug_3", ir=3, width=48, chain_type=ChainType.CaptureUpdateChain),
//         TdrConfig(name="debug_7", ir=7, width=16, chain_type=ChainType.CaptureChain),
//         TdrConfig(name="debug_1", ir=1, width=1, chain_type=ChainType.CaptureUpdateChain),
//         TdrConfig(name="debug_8", ir=8, width=0, chain_type=ChainType.ShiftChain)
//       ),
//       debug=Some(true)
//     ) //Mem registers, 3 chain type tests
//   ))
//   {
//     c => {new unit_tester(c)}
//   }
// }
