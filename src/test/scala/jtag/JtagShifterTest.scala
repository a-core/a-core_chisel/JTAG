// See LICENSE_UCB.txt for license details.

package jtag

import org.scalatest._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec

import chisel3._
import jtag._

trait ChainIOUtils  {
  this: AnyFlatSpec with ChiselScalatestTester =>
  // CaptureUpdateChain test utilities
  def nop(io: ChainIO) : Unit = {
    io.chainIn.shift.poke(0.U)
    io.chainIn.capture.poke(0.U)
    io.chainIn.update.poke(0.U)

    io.chainOut.shift.expect(0.U)
    io.chainOut.capture.expect(0.U)
    io.chainOut.update.expect(0.U)
  }

  def capture(io: ChainIO) : Unit = {
    io.chainIn.shift.poke(0.U)
    io.chainIn.capture.poke(1.U)
    io.chainIn.update.poke(0.U)

    io.chainOut.shift.expect(0.U)
    io.chainOut.capture.expect(1.U)
    io.chainOut.update.expect(0.U)
  }

  def shift(io: ChainIO, expectedDataOut: BigInt, dataIn: BigInt) : Unit = {
    io.chainIn.shift.poke(1.U)
    io.chainIn.capture.poke(0.U)
    io.chainIn.update.poke(0.U)

    io.chainOut.shift.expect(1.U)
    io.chainOut.capture.expect(0.U)
    io.chainOut.update.expect(0.U)

    io.chainOut.data.expect(expectedDataOut.U)
    io.chainIn.data.poke(dataIn.U)
  }

  def update(io: ChainIO) : Unit = {
    io.chainIn.shift.poke(0.U)
    io.chainIn.capture.poke(0.U)
    io.chainIn.update.poke(1.U)

    io.chainOut.shift.expect(0.U)
    io.chainOut.capture.expect(0.U)
    io.chainOut.update.expect(1.U)

  }

  // CaptureChain test utilities
  def nop(io: CaptureChain[Data]#ModIO) : Unit = {
    nop(io.asInstanceOf[ChainIO])
    io.capture.capture.expect(0.U)
  }

  def capture(io: CaptureChain[Data]#ModIO) : Unit = {
    capture(io.asInstanceOf[ChainIO])
    io.capture.capture.expect(1.U)
  }

  def shift(io: CaptureChain[Data]#ModIO, expectedDataOut: BigInt, dataIn: BigInt) : Unit = {
    shift(io.asInstanceOf[ChainIO], expectedDataOut, dataIn)
    io.capture.capture.expect(0.U)
  }

  def update(io: CaptureChain[Data]#ModIO) : Unit = {
    update(io.asInstanceOf[ChainIO])
    io.capture.capture.expect(0.U)
  }

  // CaptureUpdateChain test utilities
  def nop(io: CaptureUpdateChain[Data, Data]#ModIO) : Unit = {
    nop(io.asInstanceOf[ChainIO])
    io.capture.capture.expect(0.U)
    io.update.valid.expect(0.U)
  }

  def capture(io: CaptureUpdateChain[Data, Data]#ModIO) : Unit = {
    capture(io.asInstanceOf[ChainIO])
    io.capture.capture.expect(1.U)
    io.update.valid.expect(0.U)
  }

  def shift(io: CaptureUpdateChain[Data, Data]#ModIO, expectedDataOut: BigInt, dataIn: BigInt) : Unit = {
    shift(io.asInstanceOf[ChainIO], expectedDataOut, dataIn)
    io.capture.capture.expect(0.U)
    io.update.valid.expect(0.U)
  }

  def update(io: CaptureUpdateChain[Data, Data]#ModIO) : Unit = {
    update(io.asInstanceOf[ChainIO])
    io.capture.capture.expect(0.U)
    io.update.valid.expect(1.U)
  }
}

class JtagShifterSpec extends AnyFlatSpec with ChiselScalatestTester with ChainIOUtils {
  import BinaryParse._

  "JTAG bypass chain" should "work" in {
    test(JtagBypassChain()) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      nop(c.io)
      step()

      // Shift without expect
      c.io.chainIn.shift.poke(1.U)
      c.io.chainIn.data.poke(0.U)
      step()

      // Test shift functionality
      shift(c.io, 0, 0)
      step()

      shift(c.io, 0, 1)
      step()

      shift(c.io, 1, 1)
      step()

      shift(c.io, 1, 0)
      step()

      shift(c.io, 0, 0)
      step()

      c.io.chainIn.shift.poke(1.U)
      c.io.chainOut.shift.expect(1.U)
      c.io.chainOut.capture.expect(0.U)
      c.io.chainOut.update.expect(0.U)

      c.io.chainIn.data.poke(0.U)
      step()
      c.io.chainOut.data.expect(0.U)

      c.io.chainIn.data.poke(1.U)
      step()
      c.io.chainOut.data.expect(1.U)

      c.io.chainIn.data.poke(1.U)
      step()
      c.io.chainOut.data.expect(1.U)

      c.io.chainIn.data.poke(0.U)
      step()
      c.io.chainOut.data.expect(0.U)

      c.io.chainIn.data.poke(1.U)
      step()
      c.io.chainOut.data.expect(1.U)
    }
  }

  "8-bit capture chain" should "work" in {
    test(CaptureChain(UInt(8.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      // Test capture and shift
      c.io.capture.bits.poke("b01001110".U)
      capture(c.io)
      step()
      step()

      shift(c.io, 0, 1)
      step()

      shift(c.io, 1, 1)
      step()

      shift(c.io, 1, 0)
      step()

      shift(c.io, 1, 1)
      step()

      shift(c.io, 0, 1)
      step()
    }
  }

  "8-bit capture-update chain" should "work" in {
    test(CaptureUpdateChain(UInt(8.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      // Test capture and shift
      c.io.capture.bits.poke("b01001110".U)
      capture(c.io)
      step()
      step()

      c.io.chainIn.capture.poke(0.U)
      c.io.chainIn.shift.poke(1.U)
      c.io.chainOut.shift.expect(1.U)
      c.io.chainOut.capture.expect(0.U)
      c.io.chainOut.update.expect(0.U)
      c.io.capture.capture.expect(0.U)
      c.io.update.valid.expect(0.U)

      c.io.update.bits.expect("b01001110".U)  // whitebox testin.U)
      shift(c.io, 0, 1)
      step()

      c.io.update.bits.expect("b10100111".U)  // whitebox testin.U)
      shift(c.io, 1, 1)
      step()

      nop(c.io)  // for giggles
      step()

      c.io.update.bits.expect("b11010011".U)  // whitebox testing
      shift(c.io, 1, 0)
      step()

      c.io.update.bits.expect("b01101001".U)  // whitebox testing
      shift(c.io, 1, 1)
      step()

      // Test update
      update(c.io)
      c.io.update.bits.expect("b10110100".U)
      step()

      // Capture-idle-update tests
      c.io.capture.bits.poke("b00000000".U)
      capture(c.io)
      step()
      step()

      nop(c.io)
      step()
      nop(c.io)
      step()
      nop(c.io)
      step()
      nop(c.io)
      step()

      update(c.io)
      c.io.update.bits.expect("b00000000".U)
      step()

      // Capture-update tests
      c.io.capture.bits.poke("b11111111".U)
      capture(c.io)
      step()
      step()

      update(c.io)
      c.io.update.bits.expect("b11111111".U)
      step()
    }
  }

  "Vector and Bundle capture-update chains" should "be ordered properly" in {
    class InnerBundle extends Bundle {
      val d = Bool()
      val c = UInt(2.W)
    }

    class TestBundle extends Bundle {
      val b = UInt(2.W)
      val a = Bool()  // this is second to ensure Bundle order is preserved
      val c = Vec(3, Bool())
      val x = new InnerBundle()
      // Ordering should be:
      // b[1:0] a c[2] c[1] c[0] x.d x.c[1:0]
    }
    test(CaptureUpdateChain(new TestBundle)) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      val cap = c.io.capture.bits
      val upd = c.io.update.bits

      // Test capture, shift, and update
      cap.b.poke("b01".U)
      cap.a.poke(1.U)
      cap.c(2).poke(1.U)
      cap.c(1).poke(0.U)
      cap.c(0).poke(0.U)
      cap.x.d.poke(1.U)
      cap.x.c.poke("b01".U)

      capture(c.io)
      step()
      step()

      shift(c.io, 1, 1)  // capture LSB, update LSB
      step()
      shift(c.io, 0, 1)
      step()
      shift(c.io, 1, 0)
      step()
      shift(c.io, 0, 0)
      step()
      shift(c.io, 0, 0)
      step()
      shift(c.io, 1, 1)
      step()
      shift(c.io, 1, 0)
      step()
      shift(c.io, 1, 0)
      step()
      shift(c.io, 0, 1)  // capture MSB, update MSB
      step()

      update(c.io)
      upd.b.expect("b10".U)
      upd.a.expect(0.U)
      upd.c(2).expect(1.U)
      upd.c(1).expect(0.U)
      upd.c(0).expect(0.U)
      upd.x.d.expect(0.U)
      upd.x.c.expect("b11".U)
      step()
    }
  }

  "Capture-update chain with larger capture width" should "work" in {
    test(CaptureUpdateChain(UInt(4.W), UInt(2.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      c.io.capture.bits.poke("b1101".U)
      capture(c.io)
      step()
      step()

      shift(c.io, 1, 0)  // capture LSB, update LSB
      step()
      shift(c.io, 0, 1)  // update MSB
      step()
      shift(c.io, 1, 1)
      step()
      shift(c.io, 1, 1)  // capture MSB
      step()

      update(c.io)
      c.io.update.bits.expect("b10".U)
    }
  }


  "Capture-update chain with larger update width" should "work" in {
    test(CaptureUpdateChain(UInt(2.W), UInt(4.W))) { c =>

      def step(): Unit = {
        c.clock.step()
      }

      c.io.capture.bits.poke("b10".U)
      capture(c.io)
      step()
      step()

      shift(c.io, 0, 1)  // capture LSB, update LSB
      step()
      shift(c.io, 1, 0)  // capture MSB
      step()
      shift(c.io, 0, 1)
      step()
      shift(c.io, 0, 1)  // update MSB
      step()

      update(c.io)
      c.io.update.bits.expect("b1101".U)
    }
  }
}
