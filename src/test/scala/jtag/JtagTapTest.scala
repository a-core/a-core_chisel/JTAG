// See LICENSE_UCB.txt for license details.

package jtag

import org.scalatest._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec


import chisel3._
import jtag._

trait JtagTestUtilities {
  this: AnyFlatSpec with ChiselScalatestTester => 
  var expectedInstruction: Option[Int] = None  // expected instruction output after TCK low

  /** Convenience function for stepping a JTAG cycle (TCK high -> low -> high) and checking basic
    * JTAG values.
    *
    * @param tms TMS to set after the clock low
    * @param expectedState expected state during this cycle
    * @param tdi TDI to set after the clock low
    * @param expectedTdo expected TDO after the clock low
    */
  def jtagCycle(io: JtagBlockIO, tms: Int, 
                expectedState: JtagState.State,
                tdi: Bool = false.B,
                expectedTdo: Tristate = {var defT = new Tristate; defT.data := false.B; defT.driven := false.B; defT}, 
                msg: String = "") : Unit = {

    def step(): Unit = {
      io.tap.TCK.step()
    }
  
    io.tap.TCK.asBool.expect(1.U, "TCK must start at 1")
    io.output.state.expect(expectedState, s"$msg: expected state $expectedState")

    io.tap.TCK.asBool.poke(0.U)
    step()

    io.tap.TMS.poke(tms)
    io.tap.TDI.poke(tdi)
    io.output.state.expect(expectedState, s"$msg: expected state $expectedState")
    
    io.tap.TDO.expect(expectedTdo, s"$msg: TDO")
    expectedInstruction match {
      case Some(instruction) =>
        io.output.instruction.expect(instruction, s"$msg: expected instruction $instruction")
      case None =>
    }

    io.tap.TCK.asBool.poke(1.U)
    step()

    io.tap.TMS.poke(tms)
    io.tap.TDO.expect(expectedTdo, s"$msg: TDO")
  }

  /** After every TCK falling edge following this call, expect this instruction on the status line.
    * None means to not check the instruction output.
    */
  // TODO: GET RID OF THIS, BETTER WAY TO CAPTURE STATE
  def expectInstruction(expected: Option[Int]): Unit = {
    expectedInstruction = expected
  }

  /** Resets the test block using 5 TMS transitions.
    */
  def tmsReset(io: JtagBlockIO): Unit = {

    io.tap.TMS.poke(1.U)
    io.tap.TCK.asBool.poke(1.U)
    for (_ <- 0 until 5) {
      io.tap.TCK.step(1)
    }
    io.output.state.expect(JtagState.TestLogicReset, "TMS reset: expected in reset state")
  }

  def resetToIdle(io: JtagBlockIO): Unit = {
    tmsReset(io)
    jtagCycle(io, 0, JtagState.TestLogicReset)
    io.output.state.expect(JtagState.RunTestIdle)
  }

  def idleToDRShift(io: JtagBlockIO): Unit = {
    jtagCycle(io, 1, JtagState.RunTestIdle)
    jtagCycle(io, 0, JtagState.SelectDRScan)
    jtagCycle(io, 0, JtagState.CaptureDR)
    io.output.state.expect(JtagState.ShiftDR)
  }

  def idleToIRShift(io: JtagBlockIO): Unit = {   
    jtagCycle(io, 1, JtagState.RunTestIdle)
    jtagCycle(io, 1, JtagState.SelectDRScan)
    jtagCycle(io, 0, JtagState.SelectIRScan)
    jtagCycle(io, 0, JtagState.CaptureIR)
    io.output.state.expect(JtagState.ShiftIR)
  }

  def drShiftToIdle(io: JtagBlockIO): Unit = {
    jtagCycle(io, 1, JtagState.Exit1DR)
    jtagCycle(io, 0, JtagState.UpdateDR)
    io.output.state.expect(JtagState.RunTestIdle)
  }

  def irShiftToIdle(io: JtagBlockIO): Unit = {
    jtagCycle(io, 1, JtagState.Exit1IR)
    jtagCycle(io, 0, JtagState.UpdateIR)
    io.output.state.expect(JtagState.RunTestIdle)
  }

  /** Shifts data into the TDI register and checks TDO against expected data. Must start in the
    * shift, and the TAP controller will be in the Exit1 state afterwards.
    *
    * TDI and expected TDO are specified as a string of 0, 1, and ?. Spaces are discarded.
    * The strings are in time order, the first elements are the ones shifted out (and expected in)
    * first. This is in waveform display order and LSB-first order (so when specifying a number in
    * the usual MSB-first order, the string needs to be reversed).
    */
  def shift(io: JtagBlockIO, tdi: String, expectedTdo: String, expectedState: JtagState.State, expectedNextState: JtagState.State): Unit = {

    def charToTristate(x: Char): Tristate = {
      var newT = new Tristate
      x match {
        case '0' => { newT.data := 0.U; newT.driven := true.B }
        case '1' => { newT.data := 1.U; newT.driven := true.B }
        case '?' => { newT.data := 1.U; newT.driven := false.B }
      }
      newT
    }

    def charToBool(x: Char): Bool = {
      x match {
        case '0' => false.B
        case '1' => true.B
        case '?' => true.B
      }
    }

    val tdiBits = tdi.replaceAll(" ", "") map charToBool
    val expectedTdoBits = expectedTdo.replaceAll(" ", "") map charToTristate

    require(tdiBits.size == expectedTdoBits.size)
    val zipBits = tdiBits zip expectedTdoBits

    for ((tdiBit, expectedTdoBit) <- zipBits.init) {
      jtagCycle(io, 0, expectedState, tdi=tdiBit, expectedTdo=expectedTdoBit)
    }

    val (tdiLastBit, expectedTdoLastBit) = zipBits.last
    
    jtagCycle(io, 1, expectedState, tdi=tdiLastBit, expectedTdo=expectedTdoLastBit)
    
    io.output.state.expect(expectedNextState)
  }

  def drShift(io: JtagBlockIO, tdi: String, expectedTdo: String): Unit = {
    shift(io, tdi, expectedTdo, JtagState.ShiftDR, JtagState.Exit1DR)
  }

  def irShift(io: JtagBlockIO, tdi: String, expectedTdo: String): Unit = {
    shift(io, tdi, expectedTdo, JtagState.ShiftIR, JtagState.Exit1IR)
  }
}

trait JtagModule extends Module {
  val io: JtagBlockIO
}

class JtagClocked[T <: JtagModule](name: String, gen: ()=>T) extends Module {
  class Reclocked extends Module {
    val mod = Module(gen())
    val io = IO(chiselTypeOf(mod.io))
    io <> mod.io
  }

  val innerClock = Wire(Bool())
  val innerReset = Wire(Bool())
  val clockMod = withClockAndReset(innerClock.asClock, innerReset) {
    Module(new Reclocked)
  }

  val io = IO(chiselTypeOf(clockMod.io))
  io <> clockMod.io
  innerClock := io.tap.TCK.asBool
  innerReset := clockMod.io.output.tapIsInTestLogicReset


  if (clockMod.io.tap.TRSTn.isDefined) {
    clockMod.io.tap.TRSTn.get.asBool := false.B
  }
}

object JtagClocked {
  def apply[T <: JtagModule](name: String, gen: ()=>T): JtagClocked[T] = {
    new JtagClocked(name, gen)
  }
}

/* This test does not work anymore after changing TCK from Bool to Clock
class JtagTapExampleWaveformSpec extends AnyFlatSpec with ChiselScalatestTester with JtagTestUtilities  {
  "JTAG TAP with example waveforms from the spec" should "work" in {
    test(JtagClocked("exampleWaveforms", () => new JtagModule {
      // Explicitly create bypass chain instruction to ignore errors about no instructions
      val controller = JtagTapGenerator(2, Map(0 -> Module(new JtagBypassChain)))
      val io = IO(controller.cloneType)
      io <> controller
    })).withAnnotations(Seq(VerilatorBackendAnnotation)) { c =>
      import BinaryParse._

      val triOne: Tristate = {
        val newT = new Tristate
        newT.data := 1.U
        newT.driven := true.B
        newT
      }
      val triZero: Tristate = {
        val newT = new Tristate
        newT.data := 0.U
        newT.driven := true.B
        newT
      }
    
      tmsReset(c.io)
      expectInstruction(Some("11".b))
      // Test sequence in Figure 6-3 (instruction scan), starting with the half-cycle off-screen
      jtagCycle(c.io, 1, JtagState.TestLogicReset)
      jtagCycle(c.io, 0, JtagState.TestLogicReset)
      jtagCycle(c.io, 1, JtagState.RunTestIdle)
      jtagCycle(c.io, 1, JtagState.SelectDRScan)
      jtagCycle(c.io, 0, JtagState.SelectIRScan)
      jtagCycle(c.io, 0, JtagState.CaptureIR)
      jtagCycle(c.io, 0, JtagState.ShiftIR, tdi=false.B, expectedTdo=triOne)  // first two required IR capture bits
      jtagCycle(c.io, 1, JtagState.ShiftIR, tdi=false.B, expectedTdo=triZero)
      jtagCycle(c.io, 0, JtagState.Exit1IR)
      jtagCycle(c.io, 0, JtagState.PauseIR)
      jtagCycle(c.io, 0, JtagState.PauseIR)
      jtagCycle(c.io, 1, JtagState.PauseIR)
      jtagCycle(c.io, 0, JtagState.Exit2IR)
      jtagCycle(c.io, 0, JtagState.ShiftIR, tdi=true.B, expectedTdo=triZero)
      jtagCycle(c.io, 0, JtagState.ShiftIR, tdi=true.B, expectedTdo=triZero)
      jtagCycle(c.io, 0, JtagState.ShiftIR, tdi=false.B, expectedTdo=triOne)
      jtagCycle(c.io, 1, JtagState.ShiftIR, tdi=true.B, expectedTdo=triOne)
      jtagCycle(c.io, 1, JtagState.Exit1IR)
      expectInstruction(Some("10".b))
      jtagCycle(c.io, 0, JtagState.UpdateIR)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
    
      tmsReset(c.io)
      expectInstruction(Some("11".b))
      jtagCycle(c.io, 0, JtagState.TestLogicReset)
      // Test sequence in Figure 6-4 (data scan), starting with the half-cycle off-screen
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 1, JtagState.RunTestIdle)
      jtagCycle(c.io, 0, JtagState.SelectDRScan)
      jtagCycle(c.io, 0, JtagState.CaptureDR)
      jtagCycle(c.io, 0, JtagState.ShiftDR, tdi=true.B, expectedTdo=triZero)  // required bypass capture bit
      jtagCycle(c.io, 0, JtagState.ShiftDR, tdi=false.B, expectedTdo=triOne)
      jtagCycle(c.io, 1, JtagState.ShiftDR, tdi=true.B, expectedTdo=triZero)
      jtagCycle(c.io, 0, JtagState.Exit1DR)
      jtagCycle(c.io, 0, JtagState.PauseDR)
      jtagCycle(c.io, 0, JtagState.PauseDR)
      jtagCycle(c.io, 1, JtagState.PauseDR)
      jtagCycle(c.io, 0, JtagState.Exit2DR)
      jtagCycle(c.io, 0, JtagState.ShiftDR, tdi=true.B, expectedTdo=triOne)
      jtagCycle(c.io, 0, JtagState.ShiftDR, tdi=true.B, expectedTdo=triOne)
      jtagCycle(c.io, 0, JtagState.ShiftDR, tdi=false.B, expectedTdo=triOne)
      jtagCycle(c.io, 1, JtagState.ShiftDR, tdi=false.B, expectedTdo=triZero)
      jtagCycle(c.io, 1, JtagState.Exit1DR)
      jtagCycle(c.io, 0, JtagState.UpdateDR)
      jtagCycle(c.io, 0, JtagState.RunTestIdle)
      jtagCycle(c.io, 1, JtagState.RunTestIdle)
      jtagCycle(c.io, 1, JtagState.SelectDRScan)  // Fig 6-4 says "Select-IR-Scan", seems like a typo
      jtagCycle(c.io, 1, JtagState.SelectIRScan)
      jtagCycle(c.io, 1, JtagState.TestLogicReset)
      jtagCycle(c.io, 1, JtagState.TestLogicReset)
      jtagCycle(c.io, 1, JtagState.TestLogicReset)
    }
  }
}
*/
