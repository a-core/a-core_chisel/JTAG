// See LICENSE_UCB.txt for license details.

package jtag

import org.scalatest._
import chisel3._
import jtag._
import chiseltest._
import org.scalatest.flatspec.AnyFlatSpec

class ModuleIO(irLength: Int) extends JtagBlockIO(irLength, false)  {
  val reg = Output(UInt(8.W))
  
}

/* JtagClocked does not work anymore after changing TCK from Bool to Clock
class JtagTapSpec extends AnyFlatSpec with JtagTestUtilities with ChiselScalatestTester {
  "JTAG TAP" should "output a proper IDCODE" in {
    test(JtagClocked("idcode", () => new JtagModule {
      val controller = JtagTapGenerator(2, Map(), idcode=None)
      val io = IO(chiselTypeOf(controller))
      io <> controller
    })) { c =>
      import BinaryParse._

      resetToIdle(c.io)
      idleToIRShift(c.io)
      irShift(c.io, "00", "10")
      irShiftToIdle(c.io)

      idleToDRShift(c.io)
      drShift(c.io, "00000000000000000000000000000000", "1010 0000000100100011 00001000010 1".reverse)
      drShiftToIdle(c.io)
    }
  }

  "JTAG data registers" should "capture and update" in {
    test(JtagClocked("regCaptureUpdate", () => new JtagModule {
      val irLength = 4

      val reg = RegInit(42.U(8.W))

      val chain = Module(CaptureUpdateChain(UInt(8.W)))
      chain.io.capture.bits := reg
      when (chain.io.update.valid) {
        reg := chain.io.update.bits
      }

      val controller = JtagTapGenerator(irLength, Map(1 -> chain))


      val io = IO(new ModuleIO(irLength))
      io.tap <> controller.tap
      io.output <> controller.output
      io.reg := reg
    })) { c =>
      import BinaryParse._

      resetToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect(42.U)  // reset sanity check
      idleToIRShift(c.io)
      irShift(c.io, "0001".reverse, "??01".reverse)
      irShiftToIdle(c.io)

      idleToDRShift(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect(42.U)
      drShift(c.io, "00101111", "00101010".reverse)
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("hF4".U)
      idleToDRShift(c.io)
      drShift(c.io, "11111111", "00101111")
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("hFF".U)
      idleToDRShift(c.io)
      drShift(c.io, "00000000", "11111111")
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("h00".U)
    }
  }

  "JTAG TAP" should "allow multiple instructions to select the same chain for scan" in {
    test(JtagClocked("multipleIcodes", () => new JtagModule {
      val irLength = 4

      val reg = RegInit(42.U(8.W))

      val chain = Module(CaptureUpdateChain(UInt(8.W)))
      chain.io.capture.bits := reg
      when (chain.io.update.valid) {
        reg := chain.io.update.bits
      }

      val controller = JtagTapGenerator(irLength, Map(1 -> chain, 2-> chain))

      val io = IO(new ModuleIO(irLength))
      io.tap <> controller.tap
      io.output <> controller.output
      io.reg := reg
    })) { c =>
      import BinaryParse._

      resetToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect(42.U)  // reset sanity check
      idleToIRShift(c.io)
      irShift(c.io, "0001".reverse, "??01".reverse)
      irShiftToIdle(c.io)

      idleToDRShift(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect(42.U)
      drShift(c.io, "00101111", "00101010".reverse)
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("hF4".U)
      idleToDRShift(c.io)
      drShift(c.io, "11111111", "00101111")
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("hFF".U)
      idleToDRShift(c.io)
      drShift(c.io, "00000000", "11111111")
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect(0.U)

      idleToIRShift(c.io)
      irShift(c.io, "0010".reverse, "??01".reverse)
      irShiftToIdle(c.io)

      idleToDRShift(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect(0.U)
      drShift(c.io, "00101111", "00000000".reverse)
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("hF4".U)
      idleToDRShift(c.io)
      drShift(c.io, "11111111", "00101111")
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("hFF".U)
      idleToDRShift(c.io)
      drShift(c.io, "00000000", "11111111")
      drShiftToIdle(c.io)
      c.io.asInstanceOf[ModuleIO].reg.expect("h00".U)
    }
  }
}
*/
