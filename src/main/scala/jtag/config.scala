// See LICENSE_AALTO.txt for license details

package jtag.config
import net.jcazevedo.moultingyaml._
import net.jcazevedo.moultingyaml.DefaultYamlProtocol._
import scala.math.BigInt
import scala.io.Source
import chisel3._
import jtag.Chain
import java.io.{File, BufferedWriter, FileWriter}

/** Enum for chain types */
object ChainType extends Enumeration {
  type ChainType = Value
  val CaptureUpdateChain, CaptureChain, UpdateChain, ShiftChain = Value
}

/** Parameter case class for TDR registers
  * @param name name of the instruction.
  * @param description description of the instruction for documentation.
  * @param ir instruction register value corresponding to this TDR.
  * @param width width of the TDR in bits.
  * @param chain_type type of chain as chain enum.
  * @param targets nets that should be driven by this TDR.
  */
case class TdrConfig(
  name:         String,
  description:  Option[String] = None,
  ir:           Int,
  width:        Int,
  chain_type:   ChainType.ChainType,
  targets:      Option[Seq[Tuple2[String, String]]] = None
)

/** Parameter case class for ID code
  * @param manufacturer_id device manufacturer ID
  * @param part_number device part number
  * @param version device version number
  */
case class IdcodeConfig(
  manufacturer_id:  Int = 1,
  part_number:      Int = 1,
  version:          Int = 1
)

/** JTAG parameter case class
  * @param syntax_version yaml config file syntax version.
  * @param ir_width length of insctruction register in bits.
  * @param id_code device identification code configuration.
  * @param tdrs sequence of TdrConfig structures.
  * @param debug debug flag to enable the generation of debug IO ports.
  *
  * @note syntax_version must match the major version number of the release.
  *       When instantiating JtagConfig in scala code it should be set to None
  *       as there is no YAML parsing involved.
  * 
  * @example {{{
  *val config = JtagConfig(
  *  syntax_version=None,  // None for scala instantiation
  *  ir_width=8,
  *  id_code=Some(IdcodeConfig(1,1,1)),
  *  tdrs=Seq(
  *    TdrConfig(name="addr",    ir=1, width=16, chain_type=ChainType.UpdateChain),
  *    TdrConfig(name="data",    ir=2, width=8,  chain_type=ChainType.UpdateChain),
  *    TdrConfig(name="probe_0", ir=3, width=16, chain_type=ChainType.CaptureChain),
  *  ),
  *  debug=Some(true)
  *)
  *}}}
  *
  * @todo add support for jtag_module_name
  * @todo add support for jtag_instance_name
  */
case class JtagConfig(
  syntax_version:     Option[Int], // None for scala instantiation
  jtag_module_name:   Option[String] = None,
  jtag_instance_name: Option[String] = None,
  ir_width:           Int,
  id_code:            Option[IdcodeConfig],
  tdrs:               Seq[TdrConfig],
  has_trstn:          Option[Boolean] = None,
  debug:              Option[Boolean] = None
)

object JtagConfig {
  // YAML AST to scala case class conversion formats
  implicit object chainTypeFormat extends YamlFormat[ChainType.ChainType] {
    def write(ct: ChainType.ChainType) = YamlString(ct.toString)
    def read(value: YamlValue) = value match {
      case s: YamlString => s.value match {
        case "capture" => ChainType.CaptureChain
        case "update" => ChainType.UpdateChain
        case "capture_update" => ChainType.CaptureUpdateChain
        case "shift" => ChainType.ShiftChain
        case _ => deserializationError(s"Expected one of: capture | update | capture_update | shift. " +
          s"Got `${s.value}` instead.")
      }
      case _ => deserializationError(s"Expected a string, got $value")
    }
  }
  implicit val tdrConfigFormat = yamlFormat6(TdrConfig)
  implicit val idcodeConfigFormat = yamlFormat3(IdcodeConfig)
  implicit val jtagConfigFormat = yamlFormat8(JtagConfig.apply)
  
  // TODO: Update this to always match the major version number of the release
  val syntaxVersion = 2

  /** Exception type for JTAG config parsing errors */
  class JtagConfigParseException(msg: String) extends Exception(msg)

  /** Type for representing error return values from a function */
  case class Error(msg: String) {
    /** Throw a parsing exception with a debug message. */
    def except() = { throw new JtagConfigParseException(msg) }

    /** Abort program execution and print out the reason */
    def panic() = {
      System.err.println(msg)
      System.exit(-1)
    }
  }

  /** parse legal syntax version from config yaml AST */
  private[config] def parseSyntaxVersion(yamlAst: YamlValue): Either[BigInt,Error] = {
    // get version number as an integer
    val version: BigInt = yamlAst.asYamlObject.fields.get(YamlString("syntax_version")) match {
      case Some(version) => version match {
        case maybeDecimal: YamlNumber => maybeDecimal.asInstanceOf[YamlNumber].value.toBigIntExact match {
          case Some(integer) => integer
          case None => return Right(Error(s"Top-level key `syntax_version` must have an integer value. $version is not!"))
        }
        case _ => return return Right(Error(s"Top-level key `syntax_version` must have an integer value. $version is not!"))
      }
      case None => return Right(Error("Missing required top-level key: `syntax_version`."))
    }
    if (syntaxVersion != version)
      return Right(Error(s"Unsupported syntax version: $version.\n- Supported versions: $syntaxVersion"))
    Left(version)
  }

  /** Return an error if any TDRs share an ir value */
  private[config] def disallowDuplicateIrs(config: JtagConfig): Either[Unit,Error] = {
    for (pair <- config.tdrs.combinations(2)) {
      val first = pair.head
      val second = pair.last
      if (first.ir == second.ir) {
        var message = "Each TDR must have a unique value for ir! The following tdrs have the same ir (0x%x):".format(first.ir)
        message += s"\n  $first"
        message += s"\n  $second"
        return Right(Error(message))
      }
    }
    Left(())
  }

  /** Return an error if any TDR tries to configure a too large ir value */
  private[config] def disallowTooLargeIrs(config: JtagConfig): Either[Unit,Error] = {
    val maxIr = math.pow(2, config.ir_width).toInt - 1
    for (tdr <- config.tdrs) {
      if (tdr.ir > maxIr) {
        return Right(Error(s"The requested ir (0x%x) is too large to be represented in ".format(tdr.ir) +
          s"${config.ir_width} bits as configured by `ir_width: ${config.ir_width}`:\n" +
          s"  $tdr"))
      }
    }
    Left(())
  }

  /** Return an error if any TDR tries to use the ir value reserved for BYPASS instruction */
  private[config] def disallowBypassIr(config: JtagConfig): Either[Unit,Error] = {
    val maxIr = math.pow(2, config.ir_width).toInt - 1
    for (tdr <- config.tdrs) {
      if (tdr.ir == maxIr) {
        return Right(Error(s"The requested ir (0x%x) is reserved for the BYPASS instruction!".format(tdr.ir)))
      }
    }
    Left(())
  } 

  def loadFromFile(filename: String): Either[JtagConfig,Error] = {
    println(s"\nLoading jtag configuration from file: $filename")
    var fileString: String = ""
    try {
      val bufferedSource = Source.fromFile(filename)
      fileString = bufferedSource.getLines().mkString("\n")
      bufferedSource.close
    } catch {
      case e: Exception => return Right(Error(e.getMessage()))
    }
    
    // print file contents as troubleshooting info
    println("\nYAML configuration file contents:")
    println(s"```\n$fileString\n```")

    // Determine syntax version
    val yamlAst = fileString.parseYaml
    val syntaxVersion = parseSyntaxVersion(yamlAst)
    syntaxVersion match {
      case Left(value) => ()
      case Right(err) => return Right(err)
    }

    // Parse JtagConfig from YAML AST
    val config = yamlAst.convertTo[JtagConfig]

    // sanity checks that don't depend on syntax version
    disallowDuplicateIrs(config) match {
      case Right(err) => return Right(err)
      case _ => ()
    }
    disallowTooLargeIrs(config) match {
      case Right(err) => return Right(err)
      case _ => ()
    }
    disallowBypassIr(config) match {
      case Right(err) => return Right(err)
      case _ => ()
    }

    Left(config)
  }

  def saveToFile(filepath: String, config: JtagConfig) = {
    val configWithSyntaxVersion = config.copy(syntax_version = Some(syntaxVersion))
    var yamlAst = configWithSyntaxVersion.toYaml
    val yamlString = yamlAst.prettyPrint

    val file = new File(filepath)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write("# this file was generated by `jtag.config.JtagConfig.saveToFile()`\n")
    bw.write(yamlString)
    bw.close()
  }

}
