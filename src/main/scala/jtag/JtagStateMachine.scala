// See LICENSE_UCB.txt for license details.

package jtag

import chisel3._
import chisel3.util._

/** Object for TAPcontrollers StateMachines state definitions.
  */
object JtagState {
  sealed abstract class State(val id: Int) {
    def U: UInt = id.U(State.width.W)
  }

  object State {
    import scala.language.implicitConversions

    implicit def toInt(x: State) = x.id
    implicit def toBigInt(x: State):BigInt = x.id

    val all: Set[State] = Set(
      TestLogicReset,
      RunTestIdle,
      SelectDRScan,
      CaptureDR,
      ShiftDR,
      Exit1DR,
      PauseDR,
      Exit2DR,
      UpdateDR,
      SelectIRScan,
      CaptureIR,
      ShiftIR,
      Exit1IR,
      PauseIR,
      Exit2IR,
      UpdateIR
    )
    val width = log2Ceil(all.size)
    def chiselType() = UInt(width.W)
  }

  // States as described in 6.1.1.2, numeric assignments from example in Table 6-3
  /*
    TestLogicRest 	15	no effect on system logic, entered when TMS high for 5 TCK rising edges
    RunTestIdle 	12	runs active instruction (which can be idle)
    SeletDRScan		7	
    CaptureDR		6	parallel-load DR shifter when exiting this state (if required)
    ShiftDR		2	shifts DR shifter from TDI towards TDO, last shift occurs on rising edge transition out of this state
    Exit1DR		1
    PauseDR		3	pause DR shifting
    Exit2DR		0
    UpdateDR		5	parallel-load output from DR shifter on TCK falling edge while in this state (not a rule?)
    SelectIRScan	4
    CaptureIR		14	parallel-load IR shifter with fixed logic values and design-specific when exiting this state (if required)
    ShiftIR		10	shifts IR shifter from TDI towards TDO, last shift occurs on rising edge transition out of this state
    Exit1IR		9
    PauseIR		11	pause IR shifting
    Exit2IR		8
    UpdateIR		13	latch IR shifter into IR (changes to IR may only occur while in this state, latch on TCK falling edge)
  */
  case object TestLogicReset extends State(15)
  case object RunTestIdle extends State(12)
  case object SelectDRScan extends State(7)
  case object CaptureDR extends State(6)
  case object ShiftDR extends State(2)
  case object Exit1DR extends State(1)
  case object PauseDR extends State(3)
  case object Exit2DR extends State(0)
  case object UpdateDR extends State(5)
  case object SelectIRScan extends State(4)
  case object CaptureIR extends State(14)
  case object ShiftIR extends State(10)
  case object Exit1IR extends State(9)
  case object PauseIR extends State(11)
  case object Exit2IR extends State(8)
  case object UpdateIR extends State(13)
}


/** The JTAG state machine
  *
  * Implements spec 6.1.1.1a (Figure 6.1)
  *
  * Usage notes:
  * - 6.1.1.1b state transitions occur on TCK rising edge
  * - 6.1.1.1c actions can occur on the following TCK falling or rising edge
  */
class JtagStateMachine extends Module() {
  class StateMachineIO extends Bundle {
    val tms = Input(Bool())
    val currState = Output(JtagState.State.chiselType())
  }
  val io = IO(new StateMachineIO)
  
  val nextState = WireDefault(JtagState.TestLogicReset.U)
  val currState = RegNext(next=nextState, init=JtagState.TestLogicReset.U)  

  switch (currState) {
    is (JtagState.TestLogicReset.U) {
      nextState := Mux(io.tms, JtagState.TestLogicReset.U, JtagState.RunTestIdle.U)
    }
    is (JtagState.RunTestIdle.U) {
      nextState := Mux(io.tms, JtagState.SelectDRScan.U, JtagState.RunTestIdle.U)
    }
    is (JtagState.SelectDRScan.U) {
      nextState := Mux(io.tms, JtagState.SelectIRScan.U, JtagState.CaptureDR.U)    
    }
    is (JtagState.CaptureDR.U) {
      nextState := Mux(io.tms, JtagState.Exit1DR.U, JtagState.ShiftDR.U)
    }
    is (JtagState.ShiftDR.U) {
      nextState := Mux(io.tms, JtagState.Exit1DR.U, JtagState.ShiftDR.U)
    }
    is (JtagState.Exit1DR.U) {
      nextState := Mux(io.tms, JtagState.UpdateDR.U, JtagState.PauseDR.U)
    }
    is (JtagState.PauseDR.U) {
      nextState := Mux(io.tms, JtagState.Exit2DR.U, JtagState.PauseDR.U)
    }
    is (JtagState.Exit2DR.U) {
      nextState := Mux(io.tms, JtagState.UpdateDR.U, JtagState.ShiftDR.U)
    }
    is (JtagState.UpdateDR.U) {
      nextState := Mux(io.tms, JtagState.SelectDRScan.U, JtagState.RunTestIdle.U)
    }
    is (JtagState.SelectIRScan.U) {
      nextState := Mux(io.tms, JtagState.TestLogicReset.U, JtagState.CaptureIR.U)
    }
    is (JtagState.CaptureIR.U) {
      nextState := Mux(io.tms, JtagState.Exit1IR.U, JtagState.ShiftIR.U)
    }
    is (JtagState.ShiftIR.U) {
      nextState := Mux(io.tms, JtagState.Exit1IR.U, JtagState.ShiftIR.U)
    }
    is (JtagState.Exit1IR.U) {
      nextState := Mux(io.tms, JtagState.UpdateIR.U, JtagState.PauseIR.U)
    }
    is (JtagState.PauseIR.U) {
      nextState := Mux(io.tms, JtagState.Exit2IR.U, JtagState.PauseIR.U)
    }
    is (JtagState.Exit2IR.U) {
      nextState := Mux(io.tms, JtagState.UpdateIR.U, JtagState.ShiftIR.U)
    }
    is (JtagState.UpdateIR.U) {
      nextState := Mux(io.tms, JtagState.SelectDRScan.U, JtagState.RunTestIdle.U)
    }
  }
  io.currState := currState
}

