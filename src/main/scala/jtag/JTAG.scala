// See LICENSE_AALTO.txt for license details

// This Module is used to generate a JTAG TAP which can be used to control attached registers.
// JTAG generates proper chains according to the case class given in creation. These TDR chains can
// be accessed by shifting proper instruction codes to the TDI when TAP is in IRShift state
// In ShiftDR state TDO is driven and bits of the selected Test Data Register is given

package jtag
import config._

import chisel3._
import chisel3.util._
import chisel3.experimental._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}
import config.{JtagConfig, TdrConfig, ChainType, IdcodeConfig}


/** TDR IO definitions for tdr input/output vectors.
  */
class TDRIO(inWidths: Seq[UInt], outWidths: Seq[UInt]) extends Bundle {
  val in  = Input(MixedVec(inWidths))
  val out = Output(MixedVec(outWidths))
}

/** Module definition for JTAG
  *
  * @param config JtagConfig case class with proper settings
  *
  * @todo determine how to init parallel outputs to zero
  * @todo determine if user should be let to decide chain types or if they should be taken from input widths
  */
class JTAG(config: JtagConfig) extends RawModule {

  override def desiredName = "jtag"
  
  // Sort registers by icodes
  val sortedRegs = config.tdrs.sortBy((reg: TdrConfig) => { reg.ir })
  val last_instruction = sortedRegs.last.ir

  // Create empty arrays with zero width UInts
  var ins = Array.fill(last_instruction + 1)(UInt(0.W))
  var outs = Array.fill(last_instruction + 1)(UInt(0.W)) 

  // Changes zeros to right widths according to the sorted register list
  // capture          input
  // update                   output
  // capture_update   input   output
  // shift
  sortedRegs.foreach { (reg: TdrConfig) => {
    reg.chain_type match {
      case ChainType.CaptureChain => ins(reg.ir) = UInt(reg.width.W)
      case ChainType.UpdateChain => outs(reg.ir) = UInt(reg.width.W)
      case ChainType.CaptureUpdateChain => {
        ins(reg.ir) = UInt(reg.width.W)
        outs(reg.ir) = UInt(reg.width.W)
      }
      case ChainType.ShiftChain => () // ShiftChain has no parallel inputs or outputs
      case _ => throw new RuntimeException("Encountered an unknown chain type!")
    }
  }}

  var inWidths = ins.toSeq
  var outWidths = outs.toSeq

  // Init all IOs, normal io, TDR io and debug io
  val tapio = IO(Flipped(new TAPIO(config.has_trstn.getOrElse(false))))
  val tdrio = IO(new TDRIO(inWidths, outWidths)) // JTAG TDRs (parallel IO)
  val io = IO(new JtagOutput(config.ir_width))

  // Go through all tdr IOs and put them into DontCare state, Needed when using chisel3._ (See #1160)
  for (i <- 0 until outs.length){
    tdrio.out(i) := DontCare
  }

  // Clock for falling edge signals
  val TCK_falling = WireDefault((!tapio.TCK.asUInt).asClock)

  // TRSTn should be inverted at chip-level to get active-low TRSTn
  withClockAndReset(tapio.TCK, tapio.TRSTn.getOrElse(false.B)) {
    var iList = Map[BigInt, jtag.Chain]() //Map of instructions and Chains
    // Create Chains for instructions and map them together
    config.tdrs.foreach { case (reg: TdrConfig) => {
      var icode = reg.ir
      if (reg.chain_type == ChainType.CaptureUpdateChain) {
        var TDRChain = Module(CaptureUpdateChain(UInt(reg.width.W)))
        chisel3.withClock(TCK_falling) { 
          var TDRreg = RegEnable(TDRChain.io.update.bits, 0.U((reg.width).W), TDRChain.io.update.valid) 
          tdrio.out(icode) := TDRreg
        }
        TDRChain.io.capture.bits := tdrio.in(icode)
        iList += (icode -> TDRChain)
      }
      else if (reg.chain_type == ChainType.CaptureChain) {
        var TDRChain = Module(CaptureChain(UInt(reg.width.W)))
        TDRChain.io.capture.bits := tdrio.in(icode)
        tdrio.out(icode) := DontCare
        iList += (icode -> TDRChain)
      }
      else if (reg.chain_type == ChainType.UpdateChain) {
        var TDRChain = Module(UpdateChain(UInt(reg.width.W)))
        chisel3.withClock(TCK_falling) { 
          var TDRreg = RegEnable(TDRChain.io.update.bits, 0.U((reg.width).W), TDRChain.io.update.valid)
          tdrio.out(icode) := TDRreg
        }
        iList += (icode -> TDRChain)
      }
      else if (reg.chain_type == ChainType.ShiftChain) {
        var TDRChain = Module(ShiftChain(UInt(1.W)))
        tdrio.out(icode) := DontCare
        iList += (icode -> TDRChain)
      }
      else {
        throw new RuntimeException("Unknown chain type!")
      }
    }}
    // printf(p"Instructions and Chains == ${iList}\n") // to check proper chain creation

    // Generate idcode bundle value
    val idcode = WireDefault(0.U.asTypeOf(new JTAGIdcodeBundle()))
    idcode.always1 := 1.U
    config.id_code match {
      case Some(id) => {
        idcode.version    := config.id_code.get.version.U(4.W)
        idcode.partNumber := config.id_code.get.part_number.U(16.W)
        idcode.mfrId      := config.id_code.get.manufacturer_id.U(11.W)
      }
      case _ => {
        idcode.version    := 0.U(4.W)
        idcode.partNumber := 0.U(16.W)
        idcode.mfrId      := 0.U(11.W)
      }
    }

    // TAP generation
    val TAP = JtagTapGenerator(
      irLength=config.ir_width, 
      instructions=iList,
      idcode=Some(0), // IDCODE instruction (i.e not the ID code itself)
      hasTRSTn=config.has_trstn.getOrElse(false)
    )

    // Connect inputs/outputs with TAP
    TAP.idcode.get := idcode
    tapio <> TAP.tap
    io <> TAP.output
  }
}

/** Generates verilog */
object JTAG extends App with OptionParser {
  // Parse command-line arguments
  val (options, arguments) = getopts(default_opts, args.toList)
  printopts(options, arguments)

  val config_file = options("config_file")
  var jtag_config: Option[JtagConfig] = None
  JtagConfig.loadFromFile(config_file) match {
    case Left(config) => jtag_config = Some(config)
    case Right(err) => {
      System.err.println(s"\nCould not load JTAG configuration from file:\n${err.msg}")
      System.err.println("Run your JTAG configuration YAML through jtag-config-lint for more comprehensive checks!")
      System.err.println("TODO: link to jtag-config-lint repository")
      System.exit(-1)
    }
  }

  // Generate verilog
  val annos = Seq(ChiselGeneratorAnnotation(() => new JTAG(config=jtag_config.get)))
  (new ChiselStage).execute(arguments.toArray, annos)
}

/** Module-specific command-line option parser */
trait OptionParser {
  // Module specific command-line option flags
  val available_opts: List[String] = List(
      "-config_file"
  )

  // Default values for the command-line options
  val default_opts : Map[String, String] = Map(
    "config_file"->"jtag-config.yml"
  )

  /** Recursively parse option flags from command line args
   * @param options Map of command line option names to their respective values.
   * @param arguments List of arguments to parse.
   * @return a tuple whose first element is the map of parsed options to their values 
   *         and the second element is the list of arguments that don't take any values.
   */
  def getopts(options: Map[String, String], arguments: List[String]) : (Map[String, String], List[String]) = {
    val usage = s"""
      |Usage: ${this.getClass.getName.replace("$","")} [-<option> <argument>]
      |
      | Options
      |     -config_file        [String]  : Generator YAML configuration file name. Default "jtag-config.yml".
      |     -h                            : Show this help message.
      """.stripMargin

    // Parse next elements in argument list
    arguments match {
      case "-h" :: tail => {
        println(usage)
        sys.exit()
      }
      case option :: value :: tail if available_opts contains option => {
        val (newopts, newargs) = getopts(
            options ++ Map(option.replace("-","") -> value), tail
        )
        (newopts, newargs)
      }
      case argument :: tail => {
        val (newopts, newargs) = getopts(options, tail)
        (newopts, argument.toString +: newargs)
      }
      case Nil => (options, arguments)
    }
  }

  /** Print parsed options and arguments to stdout */
  def printopts(options: Map[String, String], arguments: List[String]) = {
    println("\nCommand line options:")
    options.nonEmpty match {
      case true => for ((k,v) <- options) {
        println(s"  $k = $v")
      }
      case _ => println("  None")
    }
    println("\nCommand line arguments:")
    arguments.nonEmpty match {
      case true => for (arg <- arguments) {
        println(s"  $arg")
      }
      case _ => println("  None")
    }
  }
}
